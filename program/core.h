#ifndef CORE_H
#define CORE_H

#include <stdint.h>

int64_t dec(int64_t);

void ref_dec_by(int64_t *, int64_t);

#endif
