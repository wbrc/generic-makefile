#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

int64_t inc(int64_t);

void ref_inc_by(int64_t *, int64_t);

#endif
