#include <stdio.h>
#include <stdint.h>

#include "util.h"
#include "core.h"
#include "subsystem/str.h"
#include "submodule_a/suba.h"

int main(int argc, char **argv)
{
        int64_t n = 5;
        printf("%ld\n", n);

        n = inc(4);
        printf("%ld\n", n);

        ref_inc_by(&n, 3);
        printf("%ld\n", n);

        n = dec(9);
        printf("%ld\n", n);

        ref_dec_by(&n, 2);
        printf("%ld\n", n);

        char str[25] = "Hello, World!";
        printf("%s\n", str);

        rev(str);
        printf("%s\n", str);

        printf("%d\n", afunc(4));

        return 0;
}
