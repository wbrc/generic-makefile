#include "core.h"

int64_t dec(int64_t n)
{
        return n - 1;
}

void ref_dec_by(int64_t *p, int64_t n)
{
        *p -= n;
}
