#include "util.h"

int64_t inc(int64_t n)
{
        return n + 1;
}

void ref_inc_by(int64_t *p, int64_t n)
{
        *p += n;
}
