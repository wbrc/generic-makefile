#include <string.h>
#include <stdio.h>

#include "subsystem_internal.h"

void internal_procedure(char *str)
{
        int n;
        char *front, *back, tmp;

        n = strlen(str) - 1;
        front = str;
        back = str + n * sizeof(char);

        while (front < back)
        {
                tmp = *front;
                (*front) = *back;
                *back = tmp;
                front++;
                back--;
        }
}
